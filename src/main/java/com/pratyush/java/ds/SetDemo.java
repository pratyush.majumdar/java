package com.pratyush.java.ds;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author pratyush
 */
public class SetDemo {
	/*
	 * The HashSet class implements the Set interface, backed by a hash table which is actually a HashMap instance.
	 * This class permits the null element. As it implements the Set Interface, duplicate values are not allowed.
	 * If internal capacity is 16 and load factor is 0.75 then, number of buckets will automatically get increased 
	 * when the table has 12 elements in it.
	 */
    public static void main(String args[]){
        Set<String> names = new HashSet<String>();

        names.add("Tom");
        names.add("Mary");
        names.add(null);

        System.out.println("HashSet: " + names);
        
        List<Integer> listNumbers = Arrays.asList(3, 9, 1, 4, 7, 2, 5, 3, 8, 9, 1, 3, 8, 6);
        System.out.println("List with duplicates" + listNumbers);

        //Remove Duplicates using a HashSet
        Set<Integer> uniqueNumbers = new HashSet<Integer>(listNumbers);
        System.out.println("Duplicates Removed: " + uniqueNumbers);
        
                
        /*
         * TreeSet is one of the most important implementations of the SortedSet interface.
         * The ordering of the elements is maintained by a set using their natural ordering.
         * It can also be ordered by a Comparator provided at set creation time.
         * TreeSet implements the SortedSet interface so duplicate values are not allowed
         * TreeSet does not preserve the insertion order as elements are sorted by keys
         * 
         */
        
        Set<String> treeSet = new TreeSet<String>();
        treeSet.add("D"); 
        treeSet.add("B"); 
        treeSet.add("A");
        treeSet.add("C");
        
        // Duplicates will not get insert 
        treeSet.add("C");
        
        System.out.println("\n\nTreeSet: " + treeSet);
    }
}
