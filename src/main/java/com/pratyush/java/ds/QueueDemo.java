package com.pratyush.java.ds;

import java.util.PriorityQueue;
import java.util.Queue;

public class QueueDemo {
	/*
	 * Being an interface the queue needs a concrete class for the declaration 
	 * and the most common classes are the PriorityQueue and LinkedList in Java.
	 * 
	 * Priority Queue:
	 * Every item has a priority associated with it.
	 * An element with high priority is dequeued before an element with low priority.
	 * If two elements have the same priority, they are served according to their order in the queue.
	 */
    public static void main(String args[]) {
        Queue<Integer> queue = new PriorityQueue<Integer>();
        
        queue.add(10);
        queue.add(20);
        queue.add(30);
        queue.add(40);
        queue.add(50);
        queue.add(60);

        System.out.println("Queue : " + queue);
        System.out.println("Size : " + queue.size());

        System.out.println("Removed : " + queue.remove());	//<- Removes head and throws exception if empty
        System.out.println("Poll : " + queue.poll());		//<- Removes head and return null if empty
        System.out.println("Queue : " + queue);
        System.out.println("Peek : " + queue.peek());
    }
}
