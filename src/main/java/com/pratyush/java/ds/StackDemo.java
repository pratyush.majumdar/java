package com.pratyush.java.ds;

import java.util.Stack;

/**
 *
 * @author pratyush
 */
public class StackDemo {
    public static void main(String args[]) {
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(10);
        stack.push(20);
        stack.push(30);

        System.out.println("Stack : " + stack);
        System.out.println("Size : " + stack.size());

        System.out.println("20 is found in Position : " + stack.search(20));

        System.out.println("Removed : " + stack.pop());
        System.out.println("Queue : " + stack);
        System.out.println("Peek : " + stack.peek());
    }
}
