package com.pratyush.java.ds;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author pratyush
 */
class Student {
	private int roll;
	private String name;
	private double percentage;
	
	public Student(int roll, String name, double percentage) {
		this.roll = roll;
		this.name = name;
		this.percentage = percentage;
	}

	@Override
	public String toString() {
		return "Student [roll=" + roll + ", name=" + name + ", percentage=" + percentage + "]";
	}
}

public class HashTableDemo {
	/*
	 * In HashTable null keys and null values are not allowed
	 * All methods are Synchronized so it's thread safe
	 */
    public static void main(String args[]) {
        Hashtable<Integer, String> hashTable = new Hashtable<Integer, String>();

        hashTable.put(1, "Pratyush");
        hashTable.put(2, "Majumdar");
        hashTable.put(10, "Pratyush");
        hashTable.put(20, "Majumdar");
//        hashTable.put(null, "Majumdar");	<- Not allowed
//        hashTable.put(30, null);			<- Not allowed

        System.out.println("HashTable : " + hashTable);

        /*
         * In HashMap one null key and multiple null values are allowed
         */
        HashMap<Integer, String> hashmap = new HashMap<Integer, String>();

        hashmap.put(1, "Pratyush");
        hashmap.put(2, "Majumdar1");
        hashmap.put(3, "Pratyush");
        hashmap.put(4, "Majumdar2");
        hashmap.put(null, "Majumdar3");
        hashmap.put(null, "Majumdar4");
        hashmap.put(5, null);
        hashmap.put(6, null);

        System.out.println("\n\nHashMap : " + hashmap);
        
        /*
         * LinkedHashMap is just like HashMap with an additional feature of maintaining an order of elements inserted into it.
         * It contains only unique elements
         * It may have one null key and multiple null values
         */
        
        HashMap<Integer, String> linkedHashMap = new LinkedHashMap<Integer, String>();
        linkedHashMap.put(4, "Pratyush");
        linkedHashMap.put(null, "Majumdar");
        linkedHashMap.put(3, "Skilrock");
        linkedHashMap.put(2, "Technologies");
        linkedHashMap.put(1, "Pvt. Ltd.");
        
        System.out.println("\n\nLinkedHashMap : " + linkedHashMap);
        
        /*
         * Important Collections to process HashMaps 
         */
//        Set<String> keys = hashmap.keySet();
//        Collection<Integer> values = hashmap.values(); 
//        Set<Entry<String, Integer>> entries = hashmap.entrySet();
        
        Student student1 = new Student(1, "Pratyush", 75);
        Student student2 = new Student(1, "Pratyush", 75);
//        Student student2 = student1;
        
        System.out.println("student1 == student2 " + (student1 == student2));
        System.out.println("student1.equals(student2) " + student1.equals(student2));
        System.out.println("student1.hashCode() " + student1.hashCode());
        System.out.println("student2.hashCode() " + student2.hashCode());
        
        /*
         * The map is sorted according to the natural ordering of its keys, 
         * or by a Comparator provided at map creation time, depending on which constructor is used.
         * TreeMap is not synchronized and thus is not thread-safe.
         * The algorithmic implementation is Red-Black Trees, with time complexity of O(log n)
         */
        TreeMap<Integer, Student> treeMap = new TreeMap<Integer, Student>();
        treeMap.put(2, new Student(7, "Pratyush", 37));
        treeMap.put(1, new Student(3, "Harish", 29));
        treeMap.put(3, new Student(10, "Abhishek", 38));
        
        System.out.println("\n\nTreeMap : " + treeMap);
        
        /*
         * ConcurrentHashMap class is thread-safe.
         * The underlined data structure for ConcurrentHashMap is HashTable.
         * Null insertion is not possible in ConcurrentHashMap as key or value.
         */
        HashMap<Integer, Student> hashMap = new HashMap<Integer, Student>();
        ConcurrentHashMap<Integer, Student> concurrentHashMap = new ConcurrentHashMap<Integer, Student>();
        
        hashMap.put(1, new Student(7, "Pratyush", 37));
        hashMap.put(2, new Student(3, "Harish", 29));
        hashMap.put(3, new Student(10, "Abhishek", 38));
        
        concurrentHashMap.put(1, new Student(7, "Pratyush", 37));
        concurrentHashMap.put(2, new Student(3, "Harish", 29));
        concurrentHashMap.put(3, new Student(10, "Abhishek", 38));        
        
        Iterator<Integer> iterator2 = concurrentHashMap.keySet().iterator();
        while(iterator2.hasNext()){
        	Integer key = iterator2.next();
        	if(key == 2) // ConcurrentHashMap takes care of any new entry in the map
        		concurrentHashMap.put(4, new Student(10, "Kumbhat", 38));
        }
        
        System.out.println("\n\nConcurrentHashMap : " + concurrentHashMap);
        
        Iterator<Integer> iterator1 = hashMap.keySet().iterator();
        while(iterator1.hasNext()){
        	Integer key = iterator1.next();
        	if(key == 2) // HashMap throws ConcurrentModificationException
        		hashMap.put(4, new Student(10, "Kumbhat", 38));
        }
        
        System.out.println("\n\nHashMap : " + hashMap);
    }
}
