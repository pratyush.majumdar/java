package com.pratyush.java.ds;

import java.util.Collections;
import java.util.LinkedList;

/**
 *
 * @author pratyush
 */
public class LinkedListDemo {
    public static void main(String args[]) {
        LinkedList<String> linkedList = new LinkedList<String>();

        linkedList.add("Z");
        linkedList.add("C");
        linkedList.addFirst("P");
        linkedList.addLast("Q");

        System.out.println("List : " + linkedList);
        System.out.println("Found C : " + linkedList.contains("C"));

        System.out.println("Remove First Element : " + linkedList.removeFirst());
        System.out.println("List : " + linkedList);

        Collections.sort(linkedList);
        System.out.println("Sorted List : " + linkedList);
    }
}
