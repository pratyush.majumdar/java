package com.pratyush.java.ds;

import java.util.ArrayList;
import java.util.List;

public class ArrayListDemo {
	public static void main(String[] args) {
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee(1, "Pratyush", 10000.00));
		employees.add(new Employee(2, "Majumdar", 12000.00));
		employees.add(2, new Employee(3, "Sushil", 15000.00));
		
		System.out.println("Size: " + employees.size());
		System.out.println(employees);
		
		employees.remove(0);
		System.out.println("\n\nAfter deletion, Size: " + employees.size());
		System.out.println(employees);
	}
}

class Employee {
	int employeeId;
	String employeeName;
	Double salary;
		
	public Employee(int employeeId, String employeeName, Double salary) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.salary = salary;
	}
	
	public int getEmployeeId() {
		return employeeId;
	}
	
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	
	public String getEmployeeName() {
		return employeeName;
	}
	
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	
	public Double getSalary() {
		return salary;
	}
	
	public void setSalary(Double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", employeeName=" + employeeName + ", salary=" + salary + "]";
	}
}