package com.pratyush.java.extras;

import java.util.HashMap;
/*
 * By overriding hashcode() and equals() we can use a class as a key for Hashmap  
 */

class Employee {
	private int id;
	
	public Employee(int id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id; //fixed calculation
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (id != other.id)
			return false;
		return true;
	}
}

public class CustomKeyHashmapDemo {
	public static void main(String[] args) {
		Employee employee1 = new Employee(1);
		Employee employee2 = new Employee(1);
		
		HashMap<Employee, String> map = new HashMap<Employee, String>();
		map.put(employee1, "Pratyush");
		map.put(employee2, "Pratyush");
		
		System.out.println(map.size());
		System.out.println(map.entrySet());
	}
}
