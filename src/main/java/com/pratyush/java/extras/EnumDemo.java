package com.pratyush.java.extras;

public class EnumDemo {
	enum Season {
		Summer, Winter, Spring, Autumn
	};
	
	// Enum with Customized Value
	enum TrafficSignal {
	    RED("STOP"), GREEN("GO"), ORANGE("SLOW DOWN");
		
	    // declaring private variable for getting values
	    private String action;
	    
	    // enum constructor - cannot be public or protected
	    private TrafficSignal(String action) {
	        this.action = action;
	    }
	    
	    // getter method
	    public String getAction() {
	        return this.action;
	    }
	}
	
	public static void main(String[] args) {
		Season seasons[] = Season.values();
		for (Season season : seasons) {
			System.out.println(season + " at index " + season.ordinal());
		}
		
		System.out.println(Season.valueOf("Spring"));
		
		TrafficSignal[] signals = TrafficSignal.values();
		for (TrafficSignal signal : signals) {
            System.out.println("name : " + signal.name() + " action: " + signal.getAction()); 
        }
	}
}
