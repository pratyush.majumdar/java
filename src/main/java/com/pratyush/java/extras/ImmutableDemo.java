package com.pratyush.java.extras;

import java.util.Date;

/*
 * 1. Make the class final so that it can't be override
 * 2. Make all fields private final
 * 3. Use of Wrapper classes to avoid Setters of fields
 * 4. Create private constructor
 * 5. No Setters only Getters
 */

final class ImmutableClass {
	private final Integer id;
	private final String name;
	private final Date date;
	
	private ImmutableClass(Integer id, String name, Date date) {
        this.id = id;
        this.name = name;
        this.date = new Date(date.getTime());
    }
	
	public static ImmutableClass getInstance(Integer id, String name, Date date) {
		return new ImmutableClass(id, name, date);
	}
	
	public Integer getId() {
        return id;
    }
	
	public String getName() {
        return name;
    }
	
	public Date getDate() {
        return date;
    }
	
	@Override
    public String toString() {
		return "ImmutableClass [" + id + ", " + name + ", " + date + "]";
	}	
}

public class ImmutableDemo {
	public static void main(String[] args) {
		ImmutableClass immutableClass = ImmutableClass.getInstance(1, "Pratyush", new Date());
		System.out.println(immutableClass);
	}
}
