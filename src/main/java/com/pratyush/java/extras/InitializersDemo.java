package com.pratyush.java.extras;

/*
 * 1. Static Block is called only once at class loading or if called using static keyword
 * 2. Initialize block is called every time object is created
 * 3. Constructor is called at last 
 */

public class InitializersDemo {
	static {
		System.out.println("Inside Static block1");
	}
	
	static {
		System.out.println("Inside Static block2\n\n");
	}
	
	{
		System.out.println("Inside Initializer block");
	}
	
	public InitializersDemo() {
		System.out.println("Inside Constructor");
	}	
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		InitializersDemo initializersDemo1 = new InitializersDemo();
		InitializersDemo initializersDemo2 = new InitializersDemo();
		InitializersDemo initializersDemo3 = new InitializersDemo();
	}
}
