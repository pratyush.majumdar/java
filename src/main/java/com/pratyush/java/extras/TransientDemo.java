package com.pratyush.java.extras;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/*
 * transient is a variables modifier used in serialization. At the time of serialization, 
 * if we don’t want to save value of a particular variable in a file, then we use transient keyword. 
 * When JVM comes across transient keyword, it ignores original value of the variable and save default value 
 * of that variable data type.
 */

public class TransientDemo implements Serializable {
	private static final long serialVersionUID = -5582599963782864796L;

	// Normal variables 
    private int i = 10, j = 20;
  
    // Transient variables 
    private transient int k = 30;
  
    // Use of transient has no impact here 
    private transient static int l = 40; 
    private transient final int m = 50;
    
    
	public static void main(String[] args) throws Exception {
		TransientDemo input = new TransientDemo();
		
		// serialization 
        FileOutputStream fos = new FileOutputStream("abc.txt"); 
        ObjectOutputStream oos = new ObjectOutputStream(fos); 
        oos.writeObject(input);
        oos.close();
        
        // de-serialization 
        FileInputStream fis = new FileInputStream("abc.txt"); 
        ObjectInputStream ois = new ObjectInputStream(fis);
        TransientDemo output = (TransientDemo)ois.readObject();
        ois.close();
        
        System.out.println("i = " + output.i);
        System.out.println("j = " + output.j);
        System.out.println("k = " + output.k);
        System.out.println("l = " + TransientDemo.l);
        System.out.println("m = " + output.m);
	}
}
