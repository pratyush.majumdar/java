package com.pratyush.java.generics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * E – Element (used extensively by the Java Collections Framework, for example ArrayList, Set etc.)
 * K – Key (Used in Map)
 * N – Number
 * T – Type
 * V – Value (Used in Map)
 * S, U, V etc. – 2nd, 3rd, 4th types
 * 
 */

class GenericsType<T> {
	private T t;
	
	public GenericsType() {
		
	}
	
	public T getT() {
		return this.t;
	}
	
	public void setT(T t) {
		this.t = t;
	}

	@Override
	public String toString() {
		return "GenericsType [t=" + t + "]";
	}
}

public class GenericsDemo {
	public static void main(String[] args) {
		GenericsType<String> typeString = new GenericsType<String>();
		typeString.setT("Pratyush");
		
		GenericsType<Integer> typeInteger = new GenericsType<Integer>();
		typeInteger.setT(1000);
		
		
		System.out.println(typeString);
		System.out.println(typeInteger);
		
		List<Integer> integerList= Arrays.asList(4, 5, 6, 7);
		List<Double> doubleList= Arrays.asList(4.1, 5.1, 6.1, 7.1);
		System.out.println("Interger list sum() " + sum(integerList));
		System.out.println("Double list sum() " + sum(doubleList));
		
		printOnlyIntegerClassorSuperClass(integerList);
		
		//The method printOnlyIntegerClassorSuperClass will only take 
		//Integer or its superclass objects. 
		//However if we pass list of type Double then we will get compilation error. 
//		printOnlyIntegerClassorSuperClass(doubleList);
		
		printlist(integerList);
		printlist(doubleList);
	}
	
	// 1. Upper Bounded Wild-cards
	private static double sum(List<? extends Number> list) { 
        double sum=0.0; 
        for (Number i: list) { 
            sum+=i.doubleValue();
        }
        
        return sum; 
    }
	
	// 2. Lower Bounded Wild-cards
	public static void printOnlyIntegerClassorSuperClass(List<? super Integer> list) {
        System.out.println(list); 
    }
	
	// 3. Unbounded Wild-card
	private static void printlist(List<?> list) {
		System.out.println(list);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	public void demo() {
		List list = new ArrayList();
		list.add("abc");
		list.add(new Integer(5));

		for(Object obj : list){
			//type casting leading to ClassCastException at runtime
		    String string = (String) obj; 
		}
		
		List<String> list1 = new ArrayList<String>(); 
		list1.add("abc");
//		list1.add(new Integer(5)); //compiler error

		for(String string : list1){
		     //no type casting needed, avoids ClassCastException
		}
	}
}
