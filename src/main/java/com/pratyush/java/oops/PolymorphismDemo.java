package com.pratyush.java.oops;


class Animal {
	public void sound() {
		System.out.println("Animal is making a sound");
	}
}

class Horse extends Animal {
    @Override
    public void sound(){
        System.out.println("Neigh");
    }
}

class Cat extends Animal{
    @Override
    public void sound(){
        System.out.println("Meow");
    }
}

class Area {
	public int area(int side) {
		return side*side;
	}
	
	public int area(int length, int breadth) {
		return length*breadth;
	}
}

public class PolymorphismDemo {
	public static void main(String[] args) {
		// Runtime polymorphism Demo using @Override
		Animal horse = new Horse();
		Animal cat = new Cat();
		
		horse.sound();
		cat.sound();
		
		// Runtime polymorphism Demo Overloading
		Area area = new Area();
		System.out.println("Area of square: " + area.area(10));
		System.out.println("Area of rectangle: " + area.area(10, 20));
	}
}
