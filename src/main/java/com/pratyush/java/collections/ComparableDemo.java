package com.pratyush.java.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author pratyush
 */
public class ComparableDemo {
    public static void main(String args[]) {
        List<Dog> list = new ArrayList<Dog>();

        list.add(new Dog("Shaggy", 3));
        list.add(new Dog("Lacy", 2));
        list.add(new Dog("Roger", 10));
        list.add(new Dog("Tommy", 4));
        list.add(new Dog("Tammy", 1));

        //Sorting done by overriding compareTo() method
        Collections.sort(list);

        //List Iterator
        ListIterator<Dog> listIterator = list.listIterator();
        while(listIterator.hasNext()) {
            Dog dog = listIterator.next();
            System.out.println(dog.getName() + " " + dog.getAge());
        }
    }

    static class Dog implements Comparable<Dog> {
        private String name;
        private int age;

        public Dog(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
        
        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public int compareTo(Dog dog) {
            return this.name.compareTo(dog.getName());
        }

//        @Override
//        public int compareTo(Dog dog) {
//            if(this.age == dog.getAge())
//                return 0;
//            else if(this.age > dog.getAge())
//                return 1;
//            else
//                return -1;
//        }
    }
}
