package com.pratyush.java.collections;

import java.util.Enumeration;
import java.util.Vector;

/**
 *
 * @author pratyush
 */
public class EnumerationDemo {
    
    public static void main(String args[]){
        Vector<String> vector = new Vector<String>();
        vector.add("Sun");
        vector.add("Mon");
        vector.add("Tue");
        vector.add("Wed");

        // Enumeration is used only to read the elements not for any write operations
        Enumeration<String> enumeration = vector.elements();
        while(enumeration.hasMoreElements()){
            System.out.println(enumeration.nextElement());
        }
    }    
}
