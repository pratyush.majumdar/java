package com.pratyush.java.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author pratyush
 */
public class ComparatorDemo {

    public static void main(String args[]) {
        List<Dog> list = new ArrayList<Dog>();

        list.add(new Dog("Shaggy", 3));
        list.add(new Dog("Lacy", 2));
        list.add(new Dog("Roger", 10));
        list.add(new Dog("Tommy", 4));
        list.add(new Dog("Tammy", 1));

        Collections.sort(list, new Comparator<Dog>() {
            public int compare(Dog dog1, Dog dog2) {
                return dog1.getAge()-dog2.getAge();
            }
        });

        //List Iterator (sorted by Age)
        ListIterator<Dog> listIterator1 = list.listIterator();
        while(listIterator1.hasNext()) {
            Dog dog = listIterator1.next();
            System.out.println(dog.getName() + " " + dog.getAge());
        }

        Collections.sort(list, new Comparator<Dog>() {
            public int compare(Dog dog1, Dog dog2) {
                return dog1.getName().compareTo(dog2.getName());
            }
        });

        //List Iterator (sorted by Name)
        ListIterator<Dog> listIterator2 = list.listIterator();
        while(listIterator2.hasNext()) {
            Dog dog = listIterator2.next();
            System.out.println(dog.getName() + " " + dog.getAge());
        }
    }

    static class Dog {
        private String name;
        private int age;

        public Dog() {

        }

        public Dog(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }
}
