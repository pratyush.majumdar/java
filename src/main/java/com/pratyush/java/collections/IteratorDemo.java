package com.pratyush.java.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class IteratorDemo {
	public static void main(String[] args) {
		List<Student> list = new ArrayList<Student>();
		list.add(new Student(10, "John"));
		list.add(new Student(20, "Jack"));
		list.add(new Student(30, "Doe"));
		list.add(new Student(40, "Molly"));
		
		// Has remove() method which is not available in Enumeration
		Iterator<Student> iterator = list.iterator();
		while(iterator.hasNext()) {
			Student student = iterator.next();
			if(student.getRoll() == 30)
				iterator.remove();
			else
				System.out.println(student.getRoll() + " " + student.getName());
		}
	}
}

class Student {
	int roll;
	String name;
	
	public Student(int roll, String name) {
		this.roll = roll;
		this.name = name;
	}

	public int getRoll() {
		return roll;
	}

	public void setRoll(int roll) {
		this.roll = roll;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}