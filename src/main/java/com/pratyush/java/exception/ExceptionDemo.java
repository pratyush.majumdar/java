package com.pratyush.java.exception;

public class ExceptionDemo extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5116495387373720209L;
	
	// Parameterized constructor
	ExceptionDemo(String str) { 
		super(str); 
	}

	public static void main(String[] args) throws ExceptionDemo {
		int bal = 900;
		if (bal < 1000) {
    		throw new ExceptionDemo("Balance is less than 1000");
		}else
			System.out.println("bal: " + bal);
	}
}
