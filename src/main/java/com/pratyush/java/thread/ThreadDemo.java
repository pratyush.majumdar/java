package com.pratyush.java.thread;

public class ThreadDemo {
	/*
	 * NEW->RUNNABLE->(BLOCKED/WAITING/TIMED_WAIT)->TERMINATED
	 */
	public static void main(String[] args) {
		System.out.println("Name: " + Thread.currentThread().getName() + ", Priority: "  + Thread.currentThread().getPriority());
		
		MyThread thread1 = new MyThread();
		MyThread thread2 = new MyThread();
		MyThread thread3 = new MyThread();
		
		//#1 Execution order is unpredictable
		thread1.start();
		thread2.start();
		thread3.start();
	}
}

class MyThread extends Thread {
	@Override
	public void run() {
		Thread t = Thread.currentThread();
		System.out.println("Name: " + t.getName() + ", Priority: " + t.getPriority() +  ", Hello World!!!");
	}
}