package com.pratyush.java.thread;

import java.util.concurrent.Semaphore;
/*
 * A semaphore controls access to a shared resource through the use of a counter. 
 * If the counter is greater than zero, then access is allowed. If it is zero, then access is denied. 
 * 
 * If the semaphore’s count is greater than zero, then the thread acquires a permit, 
 * which causes the semaphore’s count to be decremented. Otherwise, the thread will be blocked until a permit can be acquired.
 * When the thread no longer needs an access to the shared resource, it releases the permit, 
 * which causes the semaphore’s count to be incremented.
 * 
 */

class Queue {
	int item;
	
	// semaphoreConsumer initialized with 0 permits to ensure put() executes first    
    Semaphore semaphoreProducer = new Semaphore(1);
    Semaphore semaphoreConsumer = new Semaphore(0);
    
    public void put(int item) {
    	try {
    		// Before producer can produce an item, it must acquire a permit from semaphoreProducer 
    		semaphoreProducer.acquire();
    	}catch (InterruptedException e) {
    		e.printStackTrace();
		}
    	
    	// producer producing an item 
        this.item = item;
        System.out.println("PRODUCER Produced " + item);
        
        // After producer produces the item, it releases semaphoreConsumer to notify consumer 
        semaphoreConsumer.release();
    }
    
    public void get() {
    	try { 
            // Before consumer can consume an item, it must acquire a permit from semaphoreConsumer 
    		semaphoreConsumer.acquire(); 
        }catch(InterruptedException e) {
        	e.printStackTrace();
        }
    	
    	// consumer consuming an item 
        System.out.println("CONSUMER Consumed " + item);
        
        // After consumer consumes the item, it releases semaphoreProducer to notify producer 
        semaphoreProducer.release();
    }
}

class Prod extends Thread {
	Queue sharedQueue;
	
	public Prod(Queue sharedQueue) {
		super("PRODUCER");
		this.sharedQueue = sharedQueue;
	}
	
	public void run() {
		for(int i=0; i < 5; i++)
			sharedQueue.put(i);
	}
}

class Cons extends Thread {
	Queue sharedQueue;
	
	public Cons(Queue sharedQueue) {
		super("CONSUMER");
		this.sharedQueue = sharedQueue;
	}
	
	public void run() {
		for(int i=0; i < 5; i++)
			sharedQueue.get();		
	}
}

public class SemaphoreProducerConsumerDemo {
	public static void main(String[] args) {
		Queue sharedQueue = new Queue();
		
		Prod producer = new Prod(sharedQueue);
		Cons consumer = new Cons(sharedQueue);
		
		producer.start();
		consumer.start();
	}
}
