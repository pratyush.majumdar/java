package com.pratyush.java.thread;

import java.util.Scanner;

public class ProducerConsumerDemo {
	/* 
	 * To avoid polling, Java uses three methods, namely, wait(), notify() and notifyAll().
	 * They must be used within a synchronized block only.
	 * wait()-It tells the calling thread to give up the lock and go to sleep until some other thread enters the same monitor 
	 * and calls notify().
	 * 
	 * notify()-It wakes up one single thread that called wait() on the same object. It should be noted that calling notify() 
	 * does not actually give up a lock on a resource.
	 * 
	 * notifyAll()-It wakes up all the threads that called wait() on the same object.
	 * 
	 * HERE Producer-Consumer PROBLEM HAS BEEN SOLVED USING wait() notify() METHOD
	*/
	
	public static void main(String[] args) throws InterruptedException {
		final ProducerConsumer producerConsumer = new ProducerConsumer();
		
		Thread t1 = new Thread(new Runnable() {
			public void run() {				
				try {
					producerConsumer.produce();
				}catch (InterruptedException  e) {
					e.printStackTrace();
				}
			}
		});
		
		Thread t2 = new Thread(new Runnable() {
			public void run() {				
				try {
					producerConsumer.consume();
				}catch (InterruptedException  e) {
					e.printStackTrace();
				}
			}
		});
		
		t1.start();
		t2.start();
		
		//Here t1 finishes before t2
		t1.join();
		t2.join();
	}
	
	public static class ProducerConsumer {
		// Prints a string and waits for consume() 
		public void produce()throws InterruptedException {
			synchronized(this) {
				System.out.println("producer thread running");
				wait();
				System.out.println("Resumed"); 
			}
		}
		
		// Sleeps for some time and waits for a key press. After key 
        // is pressed, it notifies produce()
		public void consume()throws InterruptedException {
			// this makes the produce thread to run first. 
            Thread.sleep(1000); 
            Scanner s = new Scanner(System.in);
			synchronized(this) {	            
            	System.out.println("Waiting for return key."); 
                s.nextLine(); 
                System.out.println("Return key pressed"); 
  
                // notifies the produce thread that it 
                // can wake up. 
                notify(); 
  
                // Sleep 
                Thread.sleep(2000);
			}
			s.close();
		}
	}
}
