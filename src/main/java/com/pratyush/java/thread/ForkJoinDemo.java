package com.pratyush.java.thread;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class ForkJoinDemo {
	/*
	 * Forking stands for splitting one task into multiple sub tasks so that each task can be executed concurrently by individual CPUs
	 * followed by Joining the results of all the subtasks into one.
	 * +------+		+------------+	+------------+	+------------+
	 * | TASK | ->  | sub-task-1 |	| sub-task-2 |	| sub-task-3 |
	 * +------+		+------------+	+------------+	+------------+
	 * 					   |			   |			   |
	 * 					   v			   v			   v
	 * 				+--------------------------------------------+
	 * 				|					RESULT					 |
	 *				+--------------------------------------------+
	 *		Available CPUs -> CPU0, CPU1, CPU2 
	 */
	public static void main(String[] args) {
		int nThreads = Runtime.getRuntime().availableProcessors();
		System.out.println("CPUs: " + nThreads);
	  
		int[] numbers = new int[1000]; 
	
		for(int i = 0; i < numbers.length; i++) {
			numbers[i] = i;
		}
		
		ForkJoinPool forkJoinPool = new ForkJoinPool(nThreads);
		
		Sum sum = new Sum(numbers,0,numbers.length);
		Long result = forkJoinPool.invoke(sum);
		forkJoinPool.shutdown();
		
		System.out.println(result);
	}	
}

class Sum extends RecursiveTask<Long> {
	private static final long serialVersionUID = 8984460354588490815L;
	
	int low;
	int high;
	int[] array;

	public Sum(int[] array, int low, int high) {
		this.array = array;
		this.low   = low;
		this.high  = high;
	}
      
	@Override
	protected Long compute() {
		if(high - low <= 10) {
            long sum = 0;
            
            for(int i = low; i < high; ++i) 
               sum += array[i];
               return sum;
         }else {
             int mid = low + (high - low) / 2;
             Sum left  = new Sum(array, low, mid);
             Sum right = new Sum(array, mid, high);
             left.fork();
             long rightResult = right.compute();
             
             long leftResult  = left.join();
             return leftResult + rightResult;
          }
	}
}