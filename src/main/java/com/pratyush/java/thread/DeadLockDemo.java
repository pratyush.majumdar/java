package com.pratyush.java.thread;

public class DeadLockDemo {
	String o1 = "Lock ";
	String o2 = "Step ";
	
	Thread thread1 = (new Thread("Printer-1") {
		public void run() {
			while(true) {
				synchronized (o1) {
					synchronized (o2) {
						System.out.println(o1 + o2);
					}
				}
			}
		}
	});
	
	Thread thread2 = (new Thread("Printer-2") {
		public void run() {
			while(true) {
				synchronized (o2) {
					synchronized (o1) {
						System.out.println(o2 + o1);
					}
				}
			}
		}
	});
	
	public static void main(String[] args) {
		DeadLockDemo deadLockDemo = new DeadLockDemo();
		deadLockDemo.thread1.start();
		deadLockDemo.thread2.start();
	}
}
