package com.pratyush.java.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

class Task implements Runnable {
	int id;
	
	public Task(int id) {
		this.id = id;
	}
	
	public void run() {
		System.out.println(Thread.currentThread().getName() + " (Start) = " + id);
		
        //call workToBeDone method to simulate a delay
        workToBeDone();
        
        System.out.println(Thread.currentThread().getName() + " (End)   = " + id);
	}
	
	private void workToBeDone() {  
        try {  
        	Thread.sleep(5000);  
        } 
        catch (InterruptedException e) { 
        	e.printStackTrace(); 
        }  
    }
}

public class ExecutorDemo {
	public static void main(String[] args) {
//		ExecutorService executorService = Executors.newCachedThreadPool();
		ExecutorService executorService = Executors.newFixedThreadPool(4, new MyThreadFactory());
		for(int i=0;i<10;i++) {
			executorService.execute(new Task(i));
		}
		executorService.shutdown();
		
		System.out.println(Thread.currentThread().getName() + " Thread End !!!");
	}
}

class MyThreadFactory implements ThreadFactory {

	public Thread newThread(Runnable r) {
		Thread thread = new Thread(r);
		thread.setDaemon(true);
//		thread.setName("PratyushThread");
		return thread;
	}
	
}
