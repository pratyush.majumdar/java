package com.pratyush.java.thread;

public class RunnableDemo {
	/*
	 * NEW->RUNNABLE->(BLOCKED/WAITING/TIMED_WAIT)->TERMINATED
	 */
	public static void main(String[] args) {
		System.out.println("Name: " + Thread.currentThread().getName() + ", Priority: "  + Thread.currentThread().getPriority());
		
		Thread thread1 = new Thread(new MyRunnable());
		Thread thread2 = new Thread(new MyRunnable());
		Thread thread3 = new Thread(new MyRunnable());
		
		//#2 Execution order is unpredictable
		thread1.start();
		thread2.start();
		thread3.start();
	}	
}

class MyRunnable implements Runnable {
	
	public void run() {
		Thread t = Thread.currentThread();
		System.out.println("Name: " + t.getName() + ", Priority: " + t.getPriority() +  ", Hello World!!!");
	}
}