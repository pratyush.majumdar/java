package com.pratyush.java.thread;

class YieldThread extends Thread {
	/*
	 * yield() indicates that the current Thread is not doing anything important and other Thread can run.
	 * sleep() causes the Thread indefinitely stop execution for the given amount of time.
	 */
	public void run() {
		for(int i=0;i<5;i++)
			System.out.println(Thread.currentThread().getName() + " in control");
	}
}

public class YieldDemo {
	public static void main(String args[]) {
		YieldThread thread = new YieldThread();
		thread.start();
		
		for(int i=0;i<5;i++) {
			Thread.yield();
			System.out.println(Thread.currentThread().getName() + " IN control");
		}
	}
}
