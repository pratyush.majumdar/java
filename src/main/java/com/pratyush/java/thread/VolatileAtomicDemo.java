package com.pratyush.java.thread;

import java.util.concurrent.atomic.AtomicInteger;

public class VolatileAtomicDemo {
	
	/* 						Thread-1	 Thread-2
	 *					+-------------+-------------+
	 * 					|    core1	  |	   core2	|
	 * 					+-------------+-------------+
	 * 	flag = true		| local cache | local cache |	flag = false
	 * 					+-------------+-------------+
	 * 	flag = true		|		Shared Cache		|	flag = true
	 * 					+---------------------------+
	 * 
	 *  Any modifications done to flag variable by Thread-2 will not be visible to Thread-1, so
	 *  we use volatile keyword so that we can handle Inter-Thread visibility problems
	 *  
	 *  AtomicIntegers are used for counters between multiple Threads. As counter++ is not an atomic operation.
	 *  It's read, update and write. Using synchronized keyword may slow down operation
	 */
	class Counter {
		private int count;
	    AtomicInteger atomicCount = new AtomicInteger(0);
	    
	    /*
	     * This method thread-safe now because of locking and synchronization
	     */
	    public synchronized int getCount() {
	        return count++;
	    }
	  
	    /*
	     * This method is thread-safe because count is incremented atomically
	     */
	    public int getCountAtomically() {
	        return atomicCount.incrementAndGet();
	    }
	}
	
	class SharedObj {
	   // volatile keyword here makes sure that
	   // the changes made in one thread are 
	   // immediately reflected in other thread
	   volatile int sharedVar = 6;
	}
	
	public static void main(String[] args) {
		
	}
}
