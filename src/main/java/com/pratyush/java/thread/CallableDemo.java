package com.pratyush.java.thread;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/*
 * For implementing Runnable, the run() method needs to be implemented which does not return anything, 
 * while for a Callable, the call() method needs to be implemented which returns a result on completion. 
 * Note that a thread can�t be created with a Callable, it can only be created with a Runnable. 
 * 
 * Future object holds the result, it may not hold it right now, but it will do so in the future (once the Callable returns).
 * To create the thread, a Runnable is required. To obtain the result, a Future is required.
 */

class CallableExample implements Callable<Object> {
	
	public Object call() throws Exception {
		Random generator = new Random();
		Integer randomNumber = generator.nextInt(5);
		Thread.sleep(randomNumber * 1000);
		return randomNumber; 
	}
}

public class CallableDemo {
	public static void main(String[] args) throws Exception {
		FutureTask<Object> randomNumberTask = new FutureTask<Object>(new CallableExample());
		
		Thread thread = new Thread(randomNumberTask); 
		thread.start();
		
		System.out.println(randomNumberTask.get());
	}
}
