package com.pratyush.java.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

/*
 * The traditional way to achieve thread synchronization in Java is by the use of synchronized keyword.
 * Reentrant Locks are provided in Java to provide synchronization with greater flexibility.
 * ReentrantLock allow threads to enter into lock on a resource more than once. 
 * When the thread first enters into lock, a hold count is set to one. Before unlocking the thread can re-enter into lock again 
 * and every time hold count is incremented by one. 
 * 
 */
class Worker implements Runnable {
	private ReentrantLock reentrantLock;
	private String taskName;
	
	public Worker(ReentrantLock reentrantLock, String taskName) {
		this.reentrantLock = reentrantLock;
		this.taskName = taskName;
	}
	
	public void run() {
		boolean running = true;
		while(running) {
			boolean lockAquired = reentrantLock.tryLock();
			if(lockAquired) {
				try {
					System.out.println("Starting task " + taskName);
					try{Thread.sleep(1000);}catch (Exception e) {}
					running = false;
				}finally {
					// Always remember to unload the Reentrant Lock 
					reentrantLock.unlock();
				}
			} else {
				System.out.println("Waiting for " + taskName);
				try {Thread.sleep(1000);}catch (Exception e) {}
			}
		}
	}
}

public class ReentrantLockDemo {
	public static void main(String[] args) {
		ReentrantLock reentrantLock = new ReentrantLock();
		ExecutorService pool = Executors.newFixedThreadPool(2);
		
		Runnable worker1 = new Worker(reentrantLock, "Task1");
		Runnable worker2 = new Worker(reentrantLock, "Task2");
		Runnable worker3 = new Worker(reentrantLock, "Task3");
		Runnable worker4 = new Worker(reentrantLock, "Task4");
		Runnable worker5 = new Worker(reentrantLock, "task5");
		
		pool.execute(worker1);
		pool.execute(worker2);
		pool.execute(worker3);
		pool.execute(worker4);
		pool.execute(worker5);
		
		pool.shutdown();
	}
}
