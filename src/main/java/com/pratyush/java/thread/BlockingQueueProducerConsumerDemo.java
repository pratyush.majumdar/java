package com.pratyush.java.thread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/*
 * The role of the BlockingQueue is to complete the Producer/Consumer pattern
 * BlockingQueue is excellent when you want to skip the complexity involved in wait–notify statements.
 * 
 * Simple code, much more readable
 * Less error prone as we don't have to deal with any external synchronization
 */

class Producer extends Thread {
	private BlockingQueue<Integer> sharedQueue;
	
	public Producer(BlockingQueue<Integer> sharedQueue) {
		super("PRODUCER");
		this.sharedQueue = sharedQueue;
	}
	
	public void run() {
		for (int i = 0; i < 10; i++) { 
			try { 
				System.out.println(getName() + " produced " + i); 
				sharedQueue.put(i); 
				Thread.sleep(500); 
			} catch (InterruptedException e) { 
				e.printStackTrace(); 
			}
		}
	}
}

class Consumer extends Thread {
	private BlockingQueue<Integer> sharedQueue;
	
	public Consumer(BlockingQueue<Integer> sharedQueue) {
		super("CONSUMER");
		this.sharedQueue = sharedQueue; 
	}
	
	public void run() {
		try {
			while (true) {
				Integer item = sharedQueue.take();
				System.out.println(getName() + " consumed " + item); 
			}
		} catch (InterruptedException e) {
			e.printStackTrace(); 
		}
	}
}

public class BlockingQueueProducerConsumerDemo {
	public static void main(String[] args) throws InterruptedException {
		BlockingQueue<Integer> sharedQueue = new LinkedBlockingQueue<Integer>();
		
		Producer producer = new Producer(sharedQueue); 
		Consumer consumer = new Consumer(sharedQueue); 
		
		producer.start(); 
		consumer.start();
	}
}
