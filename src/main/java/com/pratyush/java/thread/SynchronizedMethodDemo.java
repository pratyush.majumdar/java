package com.pratyush.java.thread;

interface Account {
	public int getBalance();
	public void credit(int amount);
	public void debit(int amount);
}

class BankAccount implements Account {
	int balance = 0;

	public int getBalance() {
		return this.balance;
	}
	
	public synchronized void credit(int amount) {
		this.balance+= amount;
	}

	public synchronized	void debit(int amount) {
		if(this.balance >= amount)
			this.balance-=amount;
		else
			System.out.println("Error: Negative Balance");
	}
}

public class SynchronizedMethodDemo {
	public static void main(String[] args) {
		final BankAccount account = new BankAccount();
		
		//Credit Thread
		new Thread() {
			public void run() {
				while(true) {
					System.out.println(Thread.currentThread().getName() + " " + Thread.currentThread().getState() + " credit: 20");
					account.credit(20);
					try {Thread.sleep(1000);}catch(Exception e) {e.printStackTrace();}
				}
			}
		}.start();
				
		//Debit Thread
		new Thread() {
			public void run() {
				while(true) {
					System.out.println(Thread.currentThread().getName() + " " + Thread.currentThread().getState() + " debit: 30");
					account.debit(30);
					try {Thread.sleep(1000);}catch(Exception e) {e.printStackTrace();}
				}
			}
		}.start();
		
		//Balance Thread
		new Thread() {
			public void run() {
				while(true) {
					System.out.println(Thread.currentThread().getName() + " balance " + account.getBalance());
					try {Thread.sleep(500);}catch(Exception e) {e.printStackTrace();}
				}
			}
		}.start();
		
		System.out.println( "Exit " + Thread.currentThread().getName());
	}
}