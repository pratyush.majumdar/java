package com.pratyush.java.cloning;

public class DeepCloningDemo {

	{
		
	}
	
	static {
		
	}
	
	
	/* 
	 * If we want to create a deep copy of object X and place it in a new object Y 
	 * then new copy of any referenced objects fields are created and these references are placed in object Y.
	 */
	public static void main(String[] args) throws CloneNotSupportedException {
		Test1 test = new Test1(10, 20);
		Test1 clone = test.clone();
		
		System.out.println("equals() " + test.equals(clone));
		System.out.println("values of test : " + test.a + ", " + test.b + ", " + test.c.x + ", " + test.c.y);
		
		// Change in object type field of "clone" will not 
	    // be reflected in "test"(deep copy)
		clone.c.x = 30;
		clone.c.y = 40;
		
//		clone.a = 100;
//		clone.b = 200;
				
		System.out.println("values of clone : " + clone.a + ", " + clone.b + ", " + test.c.x + ", " + test.c.y);
	}
}

class InnerClass1 {
	int x = -10;
	int y = -20;
}

class Test1 implements Cloneable {
	int a;
	int b;
	InnerClass1 c = new InnerClass1();
	
	public Test1(int a, int b) {
		this.a = a;
		this.b = b;
	}
	
	public Test1 clone() throws CloneNotSupportedException {
		Test1 test1 = (Test1) super.clone();
		
		// Create a new object for the field c 
        // and assign it to shallow copy obtained, 
        // to make it a deep copy 
		test1.c = new InnerClass1();
		
		return test1;
	}
}