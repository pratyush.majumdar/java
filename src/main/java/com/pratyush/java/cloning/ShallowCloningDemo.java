package com.pratyush.java.cloning;

public class ShallowCloningDemo {
	/*
	 * In this method the fields of an old object X are copied to the new object Y. 
	 * While copying the object type field the reference is copied to Y i.e object Y will point to same location as pointed out by X.
	 * Therefore, any changes made in referenced objects in object X or Y will be reflected in other object.
	 */
	public static void main(String[] args) throws CloneNotSupportedException {
		Test2 test = new Test2(10, 20);
		Test2 clone = test.clone();
		
		System.out.println("equals() " + test.equals(clone));
		System.out.println("values of test : " + test.a + ", " + test.b + ", " + test.c.x + ", " + test.c.y);
		
		
		clone.c.x = 30;
		clone.c.y = 40;
				
		System.out.println("values of clone : " + clone.a + ", " + clone.b + ", " + test.c.x + ", " + test.c.y);
	}
}

class InnerClass2 {
	int x = -10;
	int y = -20;
}

class Test2 implements Cloneable {
	int a;
	int b;
	InnerClass2 c = new InnerClass2();
	
	public Test2(int a, int b) {
		this.a = a;
		this.b = b;
	}
	
	public Test2 clone() throws CloneNotSupportedException {
		return (Test2) super.clone();
	}
}