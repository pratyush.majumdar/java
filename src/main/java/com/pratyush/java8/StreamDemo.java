package com.pratyush.java8;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/*
 * Java 8 introduced the concept of stream that lets the developer to process data declaratively 
 * and leverage multi core architecture  
 * 
 * Stream represents a sequence of objects from a source, which supports aggregate operations.
 * 
 * With Java 8, Collection interface has two methods to generate a Stream.
 * 1. stream() − Returns a sequential stream considering collection as its source.
 * 2. parallelStream() − Returns a parallel Stream considering collection as its source.
 * 
 * https://www.tutorialspoint.com/java8/java8_streams.htm
 * 
 */

public class StreamDemo {

	public static void main(String[] args) {
		List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
		List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
		
		//Below mentioned functionalities in Java 7 would call for iterating the List and apply the required logic
		int count = (int)strings.stream().filter(string->string.isEmpty()).count();
	    System.out.println("Empty Strings: " + count);
	    
	    count = (int)strings.stream().filter(string -> string.length() == 3).count();
	    System.out.println("Strings of length three: " + count);
	    
	    List<String> filtered = strings.stream().filter(string ->!string.isEmpty()).collect(Collectors.toList());
	    System.out.println("Filtered List: " + filtered);
	    
	    String mergedString = strings.stream().filter(string ->!string.isEmpty()).collect(Collectors.joining(", "));
	    System.out.println("Merged String: " + mergedString);
	    
	    List<Integer> squaresList = numbers.stream().map( i ->i*i).distinct().collect(Collectors.toList());
	    System.out.println("\n\nDistinct Squares List: " + squaresList);
	    
	    IntSummaryStatistics stats = numbers.stream().mapToInt((x) ->x).summaryStatistics();
	    System.out.println("## Statistics ##\nHighest number in List : " + stats.getMax());
	    System.out.println("Lowest number in List : " + stats.getMin());
	    System.out.println("Sum of all numbers : " + stats.getSum());
	    System.out.println("Average of all numbers : " + stats.getAverage());
	    
	    System.out.println("Random Numbers : ");
	    Random random = new Random(); 
	    random.ints(1,10).limit(10).sorted().forEach(System.out::println);
	    
	    //parallel processing
	    count = (int) strings.parallelStream().filter(string -> string.isEmpty()).count();
	    System.out.println("\n\nEmpty String s: " + count);
	}
}
