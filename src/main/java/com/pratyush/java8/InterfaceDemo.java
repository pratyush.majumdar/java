package com.pratyush.java8;

/*
 * Java 8 introduces a new concept of default method implementation in interfaces. 
 * This capability is added for backward compatibility so that old interfaces 
 * can be used to leverage the lambda expression capability of Java 8. 
 * 
 * An interface can also have static helper methods
 * 
 */

interface A {
	void show();
	
	// Only available in Java 8, using default keyword
	default void display() {
		System.out.println("I am in Interface A");
	}
	
	// Only available in Java 8, using static keyword
	static void print() {
	      System.out.println("I am in Interface A print");
	}
}

interface B {
	void show();
	
	// Only available in Java 8, using default keyword
	default void display() {
		System.out.println("I am in Interface B");
	}
}

class C implements A, B{
	// To solve Diamond Problem we need to define abstract methods in derived classes
	public void show() {
		System.out.println("I am in Class C");
	}
	
	// To solve Diamond Problem we need to define again  
	public void display() {
		System.out.println("I am in Class C");
	}
}

class D implements A {
	public void show() {
		System.out.println("I am in Class D");
	}
}

public class InterfaceDemo {
	public static void main(String[] args) {
		C object = new C();
		object.show();
		object.display();
		
		D obj = new D();
		obj.show();
		obj.display();
		
		A.print();
	}
}
