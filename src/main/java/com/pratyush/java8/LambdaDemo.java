package com.pratyush.java8;
/*
 * Syntax of a Lambda Expression is parameter -> expression body
 * Lambda Expressions are used primarily to define inline implementation of a functional interface
 * Lambda Expressions in Java 8, reducing boiler plate code and removing Anonymous Inner classes
 * 
 * Functional programming allows programming using expressions 
 * 1. declaring functions, passing functions as arguments 
 * 2. using functions as statements
 * 
 */
interface Phone {
	void display(int i);
}

interface MathOperation {
    int operation(int a, int b);
}

public class LambdaDemo {
	public static void main(String[] args) {
		// Using Anonymous Inner class in java 7
		Phone obj = new Phone() {
			public void display(int i) {
				System.out.println("I am in Java " + i);
			}
		};
		obj.display(7);
		
		// Lambda Expressions in Java 8, reducing boiler plate code and removing Anonymous Inner classes
		Phone object = i -> System.out.println("I am in Java " + i);
		object.display(8);
		
		
		MathOperation addition = (int a, int b) -> a + b;
		MathOperation substraction = (int a, int b) -> a - b;
		MathOperation multiplication = (int a, int b) -> a * b;
		MathOperation division = (int a, int b) -> a / b;
		
		System.out.println("\n\nAddition: " + addition.operation(10, 5));
		System.out.println("Substraction: " + substraction.operation(10, 5));
		System.out.println("Multiplication: " + multiplication.operation(10, 5));
		System.out.println("Division: " + division.operation(10, 5));
	}
}
