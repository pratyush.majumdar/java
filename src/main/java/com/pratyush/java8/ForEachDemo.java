package com.pratyush.java8;

import java.util.Arrays;
import java.util.List;

public class ForEachDemo {
	public static void main(String[] args) {
		List<Integer> array = Arrays.asList(1,2,3,4,6,7);
		
		System.out.print("Using \"for\" loop : ");
		
		for(int i=0;i<array.size();i++)
			System.out.print(array.get(i) + " ");
		
		System.out.print("\n\nUsing enhanced \"for\" loop: ");
		
		for(Integer items:array) {
			System.out.print(items + " ");
		}
		
		System.out.print("\n\nUsing Java8 \"forEach\" : ");
		
		array.forEach(items -> System.out.print(items + " "));
	}
}
