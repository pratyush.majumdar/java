package com.pratyush.java8;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;

public class DateTimeDemo {
	/*
	 * Date Time API is immutable and Thread safe
	 * Date Time API has better time zone handling
	 * 
	 * In Java 7 Default Date starts from 1900, month starts from 1, and day starts from 0, so no uniformity
	 * 
	 */
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		System.out.println(calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DATE));
		System.out.println(calendar.getTime().getHours() + ":" + calendar.getTime().getMinutes() + ":" + calendar.getTime().getSeconds());
		
		LocalDate date = LocalDate.now();
		System.out.println("\n\n" + date);
		
		LocalTime time = LocalTime.now();
		System.out.println(time);
				
		LocalTime localTime = LocalTime.now(ZoneId.of("Australia/Melbourne"));
		System.out.println("Australia Time: " + localTime);
	}
}
