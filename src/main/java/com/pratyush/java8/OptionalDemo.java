package com.pratyush.java8;

import java.util.Optional;

public class OptionalDemo {

	public static void main(String[] args) {
		OptionalDemo optionalDemo = new OptionalDemo();
		
		Integer value1 = null;
	    Integer value2 = new Integer(10);
	    
	    // Optional.ofNullable - allows null parameter
	    Optional<Integer> a = Optional.ofNullable(value1);
	    
	    //Optional.of - throws NullPointerException if passed parameter is null
	    Optional<Integer> b = Optional.of(value2);
	    
	    int result = optionalDemo.sum(a,b);
	    System.out.println("(a + b) = " + result);
	}

	public int sum(Optional<Integer> a, Optional<Integer> b) {
		System.out.println("Is a is present: " + a.isPresent());
	    System.out.println("Is b is present: " + b.isPresent());
	    
	    //Optional.orElse - returns the value if present otherwise returns the default value passed.
	    Integer value1 = a.orElse(new Integer(0));
	    Integer value2 = b.get();
		return value1 + value2;
	}
}
